
create database if not exists mini_service_pos;
use mini_service_pos;

/* clean up */
drop table if exists order_details;
drop table if exists orders;
drop table if exists customers;
drop table if exists address;
drop table if exists products;


create table if not exists products
(
  id int not null AUTO_INCREMENT,
  product_id varchar(24) not null,
  name varchar(255) not null,
  description varchar(255) not null,
  photo_file varchar(500) not null,

  primary key (id)
);

create table if not exists address
(
  id int not null AUTO_INCREMENT, 
  street varchar(100) not null, 
  zipcode varchar(10),
  city varchar(100),
  state varchar(100),
  country varchar(100) not null,

  primary key (id)
);

create table if not exists customers
(
  id int not null AUTO_INCREMENT,
  first_name varchar(100) not null,
  last_name varchar(100) not null,
  email varchar(255) not null,
  cc_number varchar(50) not null,
  cc_exp_month int not null,
  cc_exp_year int not null,
  cc_exp_cvv int not null,
  default_address int not null, 

  primary key (id),
  constraint fk_customers_default_address foreign key (default_address) references address(id)
);

create table if not exists orders
(
  id int not null AUTO_INCREMENT,
  customer_id int not null, 
  amount float not null,
  order_date timestamp not null,
  delivery_address int not null,

  primary key (id),
  constraint fk_orders_delivery_address foreign key (delivery_address) references address(id),
  constraint fk_orders_customer_id foreign key (customer_id) references customers(id)
);

create table if not exists order_details
(
  id int not null AUTO_INCREMENT,
  order_id int not null, 
  product_id int not null,

  primary key (id),
  constraint fk_order_details_order_id foreign key (order_id) references orders(id),
  constraint fk_order_details_product_id foreign key (product_id) references products(id)
);

create table if not exists inventory
(
  id int not null AUTO_INCREMENT,
  product_id int not null,
  stock int not null,

  primary key (id),
  constraint fk_inventory_product_id foreign key (product_id) references products(id)
);



/* 10 products */
INSERT INTO products (product_id, name, description, photo_file) VALUES ('2XYFJ3GM2N', 'Vanilla Latte', 'A smooth blend of espresso, steamed milk, and vanilla syrup.', 'vanillaLatte.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('8BTSF6HD4K', 'Caramel Macchiato', 'Rich espresso combined with caramel syrup and steamed milk.', 'caramelMacchiato.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('5HTDJ7LM1P', 'Mocha', 'Espresso mixed with chocolate syrup and steamed milk, topped with whipped cream.', 'mocha.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('3QWLC2XR9D', 'Espresso', 'A shot of strong, black coffee with a rich and robust flavor.', 'espresso.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('9ZXCN1UE5V', 'Cappuccino', 'Equal parts espresso, steamed milk, and foamed milk for a balanced taste.', 'cappuccino.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('7JYMK4TR2H', 'Flat White', 'A blend of espresso and velvety steamed milk with a smooth finish.', 'flatWhite.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('4XWBS8LK3M', 'Americano', 'Espresso diluted with hot water for a milder flavor.', 'americano.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('6NGPH9QA8S', 'Irish Coffee', 'Hot coffee combined with Irish whiskey, sugar, and topped with cream.', 'irishCoffee.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('1BMRF5GC7X', 'Café au Lait', 'Strong coffee mixed with steamed milk for a creamy texture.', 'cafeAuLait.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('0JYVF6LS4P', 'Macchiato', 'Espresso with a small amount of steamed milk, marked with foam.', 'macchiato.png');

/* address */
INSERT INTO address (street, zipcode, city, state, country) VALUES ('1600 Parkway Drive', '94044', 'Mountain Villa', 'AW', 'North Spain');
INSERT INTO address (street, zipcode, city, state, country) VALUES ('123 Elm Street', '12345', 'Springfield', 'IL', 'East Eurdorea');
INSERT INTO address (street, zipcode, city, state, country) VALUES ('456 Maple Avenue', '67890', 'Sunnyvale', 'CA', 'Central Lupertz City');

/* customers */
INSERT INTO customers (first_name, last_name, email, cc_number, cc_exp_month, cc_exp_year, cc_exp_cvv, default_address) VALUES ('Peter', 'Mask', 'peter.mask@gmail.com', '1234-8907-9080-1234', 12, 2026, 453, 1);
INSERT INTO customers (first_name, last_name, email, cc_number, cc_exp_month, cc_exp_year, cc_exp_cvv, default_address) VALUES ('Jane', 'Doe', 'jane.doe@example.com', '5678-1234-5678-9876', 5, 2025, 123, 2);
INSERT INTO customers (first_name, last_name, email, cc_number, cc_exp_month, cc_exp_year, cc_exp_cvv, default_address) VALUES ('John', 'Smith', 'john.smith@example.com', '4321-5678-4321-8765', 10, 2024, 789, 3);

/* orders */
insert into orders (customer_id, amount, delivery_address) values (1, 145.78, 1);

/* order_details */
insert into order_details (order_id, product_id) values (1, 9);
insert into order_details (order_id, product_id) values (1, 4);
insert into order_details (order_id, product_id) values (1, 3);

/* inventory */
insert into inventory (product_id, stock) values (1, 67);
insert into inventory (product_id, stock) values (2, 12);
insert into inventory (product_id, stock) values (3, 34);
insert into inventory (product_id, stock) values (4, 67);
insert into inventory (product_id, stock) values (5, 11);
insert into inventory (product_id, stock) values (6, 56);
insert into inventory (product_id, stock) values (7, 25);
insert into inventory (product_id, stock) values (8, 23);
insert into inventory (product_id, stock) values (9, 45);
insert into inventory (product_id, stock) values (10, 12);


grant select, update, delete, insert on mini_service_pos.* TO myuser;

/* example query */
/* 
select 
  d.*, p.name, c.first_name, c.last_name 
from 
  order_details d, products p, customers c, orders o  
where d.product_id = p.id
  and d.order_id = o.id and o.customer_id = c.id; 
*/

/* verify if user created */
/*
select user, host, grant_priv from mysql.user;
show grants for 'myuser';
*/

/* ---------------------- */
/*         SQL(s)         */
/* ---------------------- */

/* [pos] select all customers sql */
/* UI would random pick a customer as the current one */
/* info would be saved on the webapp's UI store */
/* select 
    c.id, c.first_name, c.last_name, c.email, c.email, c.cc_number, c.cc_exp_month, c.cc_exp_year, c.cc_exp_cvv, 
    a.street, a.zipcode, a.city, a.state, a.country, a.id as address_id
  from 
    customers c, address a 
  where 
    c.default_address = a.id and  c.id = {id}; */

/* [pos] top 10 products sql */
/* select * from products order by product_id asc limit 10 */

/* [pos] place order */
/* insert into orders (customer_id, amount, delivery_address) values (1, 199.7, 2); */
/* per item on the order */
/* insert into order_details (order_id, product_id) values 
  (2, 1),
  (2, 10);
*/



/* [inventory] Inventory related query */
/* listing / query */
/* select i.stock, p.* from inventory i, products p where p.id = i.product_id; */

/* [inventory] update */
/* per order completed should reduce stocks by that order's details */
/* update inventory set stock = 10000 where id = 1 */

-- next step ... create the inventory code (go) accessing the same mysql_container instance mini_service_pos.inventory
