package internal

import (
	"fmt"
	"strconv"
)

func GetIntValueFromString(value string, defValue int) (i int) {
	i = defValue
	if iValue, e := strconv.ParseInt(value, 10, 32); nil == e {
		i = int(iValue)
	} else {
		fmt.Printf("failed to convert string value to int32, %v; using default value %v\n", e, defValue)
	}
	return
}
