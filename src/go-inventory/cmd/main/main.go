package main

// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// mini-service.inventory
// Copyright (C) 2024 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/beego/beego/v2/server/web"
	c "gitlab.com/quoeamaster/mini-service/inventory/pkg/controller"
	d "gitlab.com/quoeamaster/mini-service/inventory/pkg/db"
)

func main() {
	// setup a sig-term handler
	signalTermHandler()

	// assume run in project's root folder (if not update the app.conf location accordingly)
	if e := web.LoadAppConfig("ini", "conf/app.conf"); nil != e {
		fmt.Printf("failed to load config file at root conf/app.conf; reason: %s\n", e)
		os.Exit(1)
	}
	// set routing
	web.Router("/api/v1/inventory/:id", &c.InventoryController{}, "get:Get")
	web.Router("/api/v1/inventory/update/:id/:qty", &c.InventoryController{}, "post:Post")
	web.Run()
}

// signalTermHandler - the terminate, interrupt signal handler (ctrl+c use cases)
func signalTermHandler() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		fmt.Printf("[main] signal received: %s... starting server shutdown sequence...\n", sig)

		// cleanup
		if e := d.GetMySQLConnectionPool().Close(); nil == e {
			os.Exit(0)
		} else {
			panic(e)
		}
	}()
}
