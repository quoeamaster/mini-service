package model

type Inventory struct {
	Stock           int
	Id              int     `gorm:"column:id"`
	ProductId       string  `gorm:"column:product_id"`
	ProductInstance Product `gorm:"foreignKey:product_id"`
}

func (o *Inventory) TableName() string {
	return "inventory"
}

type Product struct {
	Id          int    `gorm:"column:id"`
	ProductId   string `gorm:"column:product_id"`
	Name        string
	Description string
	PhotoFile   string `gorm:"column:photo_file"`
}

func (o *Product) TableName() string {
	return "products"
}

type InventoryProduct struct {
	Stock              int
	InventoryId        int    `gorm:"column:inventory_id"`
	ProductIdId        int    `gorm:"column:product_id_id"`
	ProductId          string `gorm:"column:product_id"`
	ProductName        string `gorm:"column:product_name"`
	ProductDescription string `gorm:"column:product_description"`
	PhotoFile          string `gorm:"column:photo_file"`
}
