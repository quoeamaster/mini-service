package db

import (
	"fmt"
	"sync"
	"time"

	"github.com/knadh/koanf"
	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/file"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type MySQLConnectionPool struct {
	db *gorm.DB
}

var instance *MySQLConnectionPool
var once sync.Once

// GetMySQLConnectionPool - return the Singleton MySQLConnectionPool instance.
func GetMySQLConnectionPool() (o *MySQLConnectionPool) {
	once.Do(func() {
		instance = &MySQLConnectionPool{}
		if e := instance.setup(); nil != e {
			panic(fmt.Errorf("[db] failed to create instance: %s", e))
		}
	})
	return instance
}

// setup - prepare the connection pooling of a MySQL instance.
func (o *MySQLConnectionPool) setup() (e error) {
	// read config file (used once and discard after configuring the connection pool)
	k := koanf.New(".")
	// TODO: ./../../conf/db.yaml depends on where you start the executable...
	e = k.Load(file.Provider("./conf/db.yaml"), yaml.Parser())
	if nil != e {
		return
	}
	// create connection pool...
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%v)/%s",
		k.String("db.user"), k.String("db.password"),
		k.String("db.host"), k.Int("db.port"), k.String("db.database"))

	o.db, e = gorm.Open(mysql.Open(dsn), &gorm.Config{
		PrepareStmt: true,
	})
	if nil != e {
		return
	}
	// retrieve the underlying db for setting the pool
	if db, e2 := o.db.DB(); nil != e2 {
		e = e2
		return
	} else {
		// pool details
		db.SetConnMaxIdleTime(2)
		db.SetMaxOpenConns(5)
		db.SetConnMaxLifetime(time.Hour)
	}
	return
}

// Close - closes the actual connection pool of MySQL.
func (o *MySQLConnectionPool) Close() (e error) {
	if nil != o.db {
		realDB, e2 := o.db.DB()
		if nil != e2 {
			e = e2
			return
		}
		return realDB.Close()
	}
	return
}

func (o *MySQLConnectionPool) GetConnection() (db *gorm.DB) {
	db = o.db
	return
}
