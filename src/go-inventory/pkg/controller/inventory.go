package controller

import (
	"github.com/beego/beego/v2/server/web"
	"gitlab.com/quoeamaster/mini-service/inventory/internal"
	d "gitlab.com/quoeamaster/mini-service/inventory/pkg/db"
	m "gitlab.com/quoeamaster/mini-service/inventory/pkg/model"
)

const (
	ControllerPathID  string = ":id"
	ControllerPathQty string = ":qty"
	PathIDValueAll    string = "all"
)

type InventoryController struct {
	// embedded the web.controller interface to the InventoryController (decorator pattern)
	web.Controller
}

func (o *InventoryController) Get() {
	// db instance; no need to close and instead wait for sig term to close the pool.
	db := d.GetMySQLConnectionPool().GetConnection()
	targetID := o.Ctx.Input.Param(ControllerPathID)

	// is it a simple get-all or get-by-id query
	if PathIDValueAll == targetID {
		// TODO: foreign key not work...??
		// var inventoryList []m.Inventory
		// db.Find(&inventoryList)

		// o.Data["json"] = inventoryList
		// o.ServeJSON()
		var inventoryList []m.InventoryProduct
		db.Table("inventory").
			Select("inventory.stock, inventory.id as inventory_id, products.id product_id_id, products.product_id product_id, products.name product_name, products.description product_description, products.photo_file").
			Joins("inner join products on products.id = inventory.product_id").
			Scan(&inventoryList)

		o.Data["json"] = inventoryList
		o.ServeJSON()

	} else {
		var inventoryItem m.InventoryProduct
		db.Table("inventory").
			Select("inventory.stock, inventory.id as inventory_id, products.id product_id_id, products.product_id product_id, products.name product_name, products.description product_description, products.photo_file").
			Joins("inner join products on products.id = inventory.product_id and inventory.id = ?", targetID).
			Scan(&inventoryItem)

		o.Data["json"] = inventoryItem
		o.ServeJSON()
	}
}

func (o *InventoryController) Post() {
	db := d.GetMySQLConnectionPool().GetConnection()
	targetID := o.Ctx.Input.Param(ControllerPathID)
	qty := o.Ctx.Input.Param(ControllerPathQty)
	// convert to int value
	iTargetId := internal.GetIntValueFromString(targetID, 1)
	iQty := internal.GetIntValueFromString(qty, 1)

	e := db.Model(&m.Inventory{Id: iTargetId}).Update("stock", iQty).Error
	if nil != e {
		panic(e)
	}
	o.Data["json"] = map[string]interface{}{
		"message": "the inventory item has been updated successfully.",
	}
	o.ServeJSON()
}
