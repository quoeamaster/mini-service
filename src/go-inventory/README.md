# API endpoint(s)

- web.Router("/api/v1/inventory/:id", &c.InventoryController{}, "get:Get")
- web.Router("/api/v1/inventory/update/:id/:qty", &c.InventoryController{}, "post:Post")

e.g. http://localhost:18101/api/v1/inventory/1
e.g. http://localhost:18101/api/v1/inventory/update/1/45
