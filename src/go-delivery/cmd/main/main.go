package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/beego/beego/v2/server/web"
	"github.com/beego/beego/v2/server/web/context"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// mini-service.inventory
// Copyright (C) 2024 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

var metricRequestCounter *prometheus.CounterVec
var metricDurationHistogram *prometheus.HistogramVec
var metricRegistry *prometheus.Registry

func main() {
	signalTermHandler()

	// TODO: update this to reflect where to load the config file
	if e := web.LoadAppConfig("ini", "./conf/app.conf"); nil != e {
		panic(fmt.Sprintf("failed to load config: %v", e))
	}
	app := web.NewHttpSever()

	// register a handler to include prometheus metrics registry
	app.Handler("/metrics", promhttp.HandlerFor(metricRegistry, promhttp.HandlerOpts{}))
	app.InsertFilter("*", web.BeforeRouter, func(ctx *context.Context) {
		start := time.Now()
		handler := ctx.Request.URL.Path
		method := ctx.Request.Method

		duration := time.Since(start).Seconds()
		metricRequestCounter.WithLabelValues(handler, method).Inc()
		metricDurationHistogram.WithLabelValues(handler, method).Observe(duration)
	})

	//app.Run("0.0.0.0:18202")
	app.Run("")
}

// init - setup the prometheus metrics.
func init() {
	metricRegistry = prometheus.NewRegistry()
	// setup the metrics
	metricRequestCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "Total number of HTTP requests",
		},
		[]string{"handler", "method"},
	)
	metricDurationHistogram = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "http_request_duration_seconds",
			Help:    "A histogram of request latencies.",
			Buckets: prometheus.DefBuckets,
		},
		[]string{"handler", "method"},
	)
	// registry registration
	// a. custom metrics
	metricRegistry.MustRegister(metricRequestCounter, metricDurationHistogram)
	// b. default metrics (kind of similar to node_exporter's capabilities)
	metricRegistry.MustRegister(
		collectors.NewGoCollector(),
		collectors.NewProcessCollector(collectors.ProcessCollectorOpts{}),
	)
}

// prometheusMiddleWare - middleware filter for custom metrics collector.
// func prometheusMiddleWare(next http.Handler) http.Handler {
// 	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		start := time.Now()
// 		handler := r.URL.Path
// 		method := r.Method

// 		// After request is processed
// 		defer func() {
// 			duration := time.Since(start).Seconds()
// 			metricRequestCounter.WithLabelValues(handler, method).Inc()
// 			metricDurationHistogram.WithLabelValues(handler, method).Observe(duration)
// 		}()
// 		next.ServeHTTP(w, r)
// 	})
// }

func signalTermHandler() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		fmt.Printf("[main] signal received: %s... starting server shutdown sequence...\n", sig)

		// cleanup
		// if e := d.GetMySQLConnectionPool().Close(); nil == e {
		// 	os.Exit(0)
		// } else {
		// 	panic(e)
		// }
		os.Exit(0)
	}()
}

// prompt % sudo lsof -i :18202
// Password:

// COMMAND   PID              USER   FD   TYPE             DEVICE SIZE/OFF NODE NAME
// main    35551 wealthyluckyjason    5u  IPv6 0xee4b7f14637b1f89      0t0  TCP *:18202 (LISTEN)

// prompt % kill -9 35551
