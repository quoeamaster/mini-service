# what is delivery service?

in general, an in-mem delivery management system; the order(s) placed would be in-mem stored and completed at a random interval; the corresponding delivery details would ONLY be available within the running of this service.

in some sense, if the delivery items are empty; there is an API to random generate some delivery histories as well.

## concept on mimicking delivery status

a list of pre-defined order and its details would be available in memory (100 entries for example); then a random pick of details would be marked as in-progress (take an example, every 1 minute would pick a random entry). This picked entry would be available on Redis; after another period, one of such in-progress entry would be picked and marked as completed in Redis (removed from the in-progress category to completed category).

### in-progress redis key

```json
order_id:abc-1234:status:in-progress
```

__order_id__ is id field value to identify a particular entry.
__status__ is for categorization purpose only; as Redis has no "table" concept; the key is how to categorize and group related entries together.

actual value associated would be the json representing the order itself; including order items, date of order, date of delivery (timestamp) and address.

### completed redis key

```json
order_id:abc-1234:status:completed
```

actual value associated would be the json representing the order itself; including order items, date of order, date of delivery (timestamp) and address.

