# [wrong syntax] go test -cover -coverprofile=coverage.o -coverpkg=./pkg/...,./cmd/...,./internal/...

# => verbose
# ./test_coverage.sh v

# => quiet
# ./test_coverage.sh

args=("$@")
flag=-${args[0]}
if [[ ${flag} == '-' ]]; then
  flag=''
fi
echo ${flag}

# create coverage_report folder if not exists
if ! test -d ./coverage_report; then
  mkdir coverage_report
fi

# remove previous coverage report files
if test -f ./coverage_report/coverage.o; then
  rm ./coverage_report/coverage.o
fi
if test -f ./coverage_report/coverage.html; then
  rm ./coverage_report/coverage.html
fi

#go test -cover -coverprofile=coverage.o -coverpkg=./... ./pkg/... ./cmd/... ./internal/...
go test ${flag} -cover -coverprofile=./coverage_report/coverage.o -coverpkg=./pkg/...,./cmd/...,./internal/...,./internal/test/... ./...
go tool cover -html=./coverage_report/coverage.o -o ./coverage_report/coverage.html
