
import yaml

def singleton(cls):
    instances = {}
    def get_instance(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]
    return get_instance    

@singleton
class ConfigReader():
    configs: any

    def __init__(self, filename):
        with open(filename, 'r') as cfile:
            self.configs = yaml.safe_load(cfile)

    def get_configs(self):
        return self.configs
    
    def get_config_by_key(self, key):
        return self.configs[key]



