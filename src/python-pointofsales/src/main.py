"""
main

This module acts as the entrypoint of the point-of-sales service.
"""

#import signal
from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from routes import general, service
from config.config import ConfigReader

# setup the routes
app = FastAPI()
app.include_router(general.router, prefix="/api", tags=["general"])
app.include_router(service.router, prefix="/api", tags=["service"])

# setup CORS
cors_config = ConfigReader('config.yml').get_config_by_key("cors")
app.add_middleware(
    CORSMiddleware,
    allow_origins=cors_config['allow_origins'],
    allow_credentials=cors_config['allow_credentials'],
    allow_methods=cors_config['allow_methods'],
    allow_headers=cors_config['allow_headers']
)

# seems... not work...
@app.exception_handler(HTTPException)
async def general_exception_handler(request, e: HTTPException):
    """Handles the missed out REST api endpoint(s). Not work though...
    
    Args:
        request (any): the http request object provided by fastAPI.
        e (HTTPException): the associated http exception.
    """
    if e.status_code == 404:
        return JSONResponse(
            status_code=404,
            content={"message": "route not found / supported"}
        )
    return JSONResponse(
        status_code=500,
        content={"message": e.detail}
    )

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=18000)
