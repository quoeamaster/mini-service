from fastapi import APIRouter
from model.generalMessage import GeneralMessage

router = APIRouter()

@router.get("", response_model=GeneralMessage, status_code=200)
@router.get("/", response_model=GeneralMessage, status_code=200)
async def landing_instruction():
    """Kind of help page."""
    return {
        "message_value": "point-of-sales application provides API endpoints for making an order plus updating inventory and delivery details based on the order",
        "message_type": "help",
        "status_code": 200
    }


