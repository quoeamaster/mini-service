"""
service

This module provides REST api handlers for the shop UI. It interacts with various services such as 
Inventory and Delivery.
"""

from fastapi import APIRouter
from typing import List
import requests
import signal
import random

from model.serviceModels import Product, to_products_from_dict
from db.point_of_sales_dao import PointOfSalesDAO
from config.config import ConfigReader

# setup the DAO and connectionPool
d_config = ConfigReader('config.yml').get_config_by_key("db")
db_params = {
    #'host': 'mysql_container',
    'host': d_config['host'],
    'user': d_config['user'],
    'password': d_config['password'],
    'database': d_config['database'],
    'autocommit': d_config['autocommit'],
}
pos_dao = PointOfSalesDAO(db_params=db_params)

# replace the dummy testing api endpoint with actual DB operations
# from constants.integrations import INVENTORY_TOP_10_PRODUCTS

router = APIRouter()

def handle_exit(sig, frame):
    """Life cycle hook to bind to terminate and interrupt"""
    if pos_dao:
        pos_dao.cleanup()
    print(f"within service.py -> {pos_dao}")
    exit(0)

# signal term, interrupt
signal.signal(signal.SIGTERM, handle_exit)
signal.signal(signal.SIGINT, handle_exit)


@router.get("/top10Products", response_model=List[Product])
async def get_top10_products():
    """Query for the top 10 products within the shop."""
    # try:
    #     headers = {}
    #     res = requests.get(INVENTORY_TOP_10_PRODUCTS, headers=headers)
    #     res.raise_for_status() # in case non 2xx status_code
    #     return res.json()
    # except requests.exceptions.HTTPError as e:
    #     return {
    #         "message": f'error found: {e}',
    #         "status_code": 500
    #     }
    # except Exception as e:
    #     return {
    #         "message": f'error found: {e}',
    #         "status_code": 500
    #     }

    # use DB instead of api testing endpoint(s)
    rand_order = random.randint(0, 2)
    order = "asc"
    if rand_order == 1:
        order = "desc"

    results = pos_dao.run_query(f"select * from products order by product_id {order} limit 10")
    # convert back to model
    models = to_products_from_dict(results=results)
    
    return models


