
import pymysql
from dbutils.pooled_db import PooledDB

class ConnectionPool:
    pool: PooledDB

    def __init__(self, db_params):
       """constructor, create the pool based on db_params

        Args:
          db_params (any): the params for connecting to the database.
       """
       self.pool = PooledDB(creator=pymysql, maxconnections=5, **db_params)
       
    def cleanup(self):
       """cleanup the resources claimed for the connection-pool."""
       if self.pool:
          self.pool.close()

    def get_connection(self):
      """return a valid db connection."""
      return self.pool.connection()
