
from db.dao_interface import AbstractMySQLDAO

class PointOfSalesDAO(AbstractMySQLDAO):
    def connect(self):
        # no implementation for this DAO
        pass
    
    def disconnect(self):
        # no implementation for this DAO
        pass
    
    def get_connection(self):
        return self.pool.get_connection()
