
from abc import ABC, abstractmethod
from db.connection_pool import ConnectionPool
from pymysql.cursors import DictCursor

class DAOInterface(ABC):
    """interface for abstracting a DAO"""

    @abstractmethod
    def connect(self):
        """connect handler.
        
        Args:
          self (any): the class instance associated.
        """
        pass
    
    @abstractmethod
    def disconnect(self):
        """disconnect handler.
        
        Args:
          self (any): the class instance associated.
        """
        pass
    
    @abstractmethod
    def get_connection(self):
        """method to get an active connection.
        
        Args:
          self (any): the class instance associated.
        """
        pass
    
    @abstractmethod
    def run_query(self, query: str):
        """execution of query.
        
        Args:
          self (any): the class instance associated.
          query (str): the query in string.
        
        Returns:
          tuple: the tuples of all related data. (non optimized as fetching everything ... imagine 1,000,000,000 rows to be fetched)
        """
        pass
    
    @abstractmethod
    def run_dml(self, query: str):
        """execution of ddl sql (e.g. insert, update, delete).
        
        Args:
          self (any): the class instance associated.
          query (str): the query in string.
        """
        pass

class DBParams:
    """structure encapsulating a DB connection paramter.
    
    Attributes:
      host (str): db hostname.
      user (str): username for connection.
      password (str): password for connection.
      db (str): database for connection.
      autocommit (bool): should autocommit? default is True.
    """
    host: str
    user: str
    password: str
    db: str
    autocommit: bool = True

class AbstractMySQLDAO(DAOInterface):
    """Abstract implementation of DAOInterface.
    
    Attributes:
      params (DBParams): structure for db connection.
      pool (ConnectionPool): connection pool for db connections.
    """
    params: DBParams
    pool: ConnectionPool

    def __init__(self, db_params):
        self.params = db_params
        self.pool = ConnectionPool(db_params=db_params)

    # def __del__(self): # do whatever destructor should do
    @abstractmethod
    def connect(self):
        pass
    
    @abstractmethod
    def disconnect(self):
        pass
    
    @abstractmethod
    def get_connection(self):
        pass
    
    def cleanup(self):
        if self.pool:
            self.pool.cleanup()

    def run_query(self, query: str):
        connection = self.get_connection()
        if connection:
            try:    
              with connection.cursor(cursor=DictCursor) as cursor:
                  cursor.execute(query)
                  results = cursor.fetchall()
                  return results
            finally:
                connection.close()
    
    def run_dml(self, query: str):
        connection = self.get_connection()
        if connection:
            try:    
              with connection.cursor(cursor=DictCursor) as cursor:
                  cursor.execute(query)
            finally:
                connection.close()
        



    
