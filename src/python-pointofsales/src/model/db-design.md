# point-of-sales db schema

For simplicity, the db would be a traditional RDBMS. Because of such, the actual db could be MySQL, Oracle, MSSQL or equivalent. An interface would be introduced on top; hence it is possible to switch between the actual db for storing the application's states.

Also to avoid proprietary db features; the schema would be designed in a conservative manner avoiding the usage of db specific data-types.

## db deployment approach

To mimick actal micro-service environment; the db engine (whatever brand it is, however here I have chosen MySQL) would be available in a container fashion. To make things simple, Docker would be chosen. Check the "docker-manifests" folder on how to deploy the MySQL instance.

## schema

The database would be mini-service-pos
```sql
create database if not exists mini_service_pos;
use mini_service_pos;

/* clean up */
drop table if exists order_details;
drop table if exists orders;
drop table if exists customers;
drop table if exists address;
drop table if exists products;
```

### products
```sql
create table if not exists products
(
  id int not null AUTO_INCREMENT,
  product_id varchar(24) not null,
  name varchar(255) not null,
  description varchar(255) not null,
  photo_file varchar(500) not null,

  primary key (id)
);
```

### addresses
```sql
create table if not exists address
(
  id int not null AUTO_INCREMENT, 
  street varchar(100) not null, 
  zipcode varchar(10),
  city varchar(100),
  state varchar(100),
  country varchar(100) not null,

  primary key (id)
);
```

### customers
```sql
create table if not exists customers
(
  id int not null AUTO_INCREMENT,
  first_name varchar(100) not null,
  last_name varchar(100) not null,
  email varchar(255) not null,
  cc_number varchar(50) not null,
  cc_exp_month int not null,
  cc_exp_year int not null,
  cc_exp_cvv int not null,
  default_address int not null, 

  primary key (id),
  constraint fk_default_address foreign key (default_address) references address(id)
);
```

### orders
```sql
create table if not exists orders
(
  id int not null AUTO_INCREMENT,
  customer_id int not null, 
  amount float not null,
  delivery_address int not null,

  primary key (id),
  constraint fk_delivery_address foreign key (delivery_address) references address(id),
  constraint fk_customer_id foreign key (customer_id) references customers(id)
);
```

### order_details
```sql
create table if not exists order_details
(
  id int not null AUTO_INCREMENT,
  order_id int not null, 
  product_id int not null,

  primary key (id),
  constraint fk_order_id foreign key (order_id) references orders(id),
  constraint fk_product_id foreign key (product_id) references products(id)
);
```

## testing data

an optional set of testing data provided as is...

```sql
/* 10 products */
INSERT INTO products (product_id, name, description, photo_file) VALUES ('2XYFJ3GM2N', 'Vanilla Latte', 'A smooth blend of espresso, steamed milk, and vanilla syrup.', 'vanillaLatte.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('8BTSF6HD4K', 'Caramel Macchiato', 'Rich espresso combined with caramel syrup and steamed milk.', 'caramelMacchiato.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('5HTDJ7LM1P', 'Mocha', 'Espresso mixed with chocolate syrup and steamed milk, topped with whipped cream.', 'mocha.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('3QWLC2XR9D', 'Espresso', 'A shot of strong, black coffee with a rich and robust flavor.', 'espresso.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('9ZXCN1UE5V', 'Cappuccino', 'Equal parts espresso, steamed milk, and foamed milk for a balanced taste.', 'cappuccino.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('7JYMK4TR2H', 'Flat White', 'A blend of espresso and velvety steamed milk with a smooth finish.', 'flatWhite.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('4XWBS8LK3M', 'Americano', 'Espresso diluted with hot water for a milder flavor.', 'americano.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('6NGPH9QA8S', 'Irish Coffee', 'Hot coffee combined with Irish whiskey, sugar, and topped with cream.', 'irishCoffee.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('1BMRF5GC7X', 'Café au Lait', 'Strong coffee mixed with steamed milk for a creamy texture.', 'cafeAuLait.png');
INSERT INTO products (product_id, name, description, photo_file) VALUES ('0JYVF6LS4P', 'Macchiato', 'Espresso with a small amount of steamed milk, marked with foam.', 'macchiato.png');

/* address */
INSERT INTO address (street, zipcode, city, state, country) VALUES ('1600 Parkway Drive', '94044', 'Mountain Villa', 'AW', 'North Spain');
INSERT INTO address (street, zipcode, city, state, country) VALUES ('123 Elm Street', '12345', 'Springfield', 'IL', 'East Eurdorea');
INSERT INTO address (street, zipcode, city, state, country) VALUES ('456 Maple Avenue', '67890', 'Sunnyvale', 'CA', 'Central Lupertz City');

/* customers */
INSERT INTO customers (first_name, last_name, email, cc_number, cc_exp_month, cc_exp_year, cc_exp_cvv, default_address) VALUES ('Peter', 'Mask', 'peter.mask@gmail.com', '1234-8907-9080-1234', 12, 2026, 453, 1);
INSERT INTO customers (first_name, last_name, email, cc_number, cc_exp_month, cc_exp_year, cc_exp_cvv, default_address) VALUES ('Jane', 'Doe', 'jane.doe@example.com', '5678-1234-5678-9876', 5, 2025, 123, 2);
INSERT INTO customers (first_name, last_name, email, cc_number, cc_exp_month, cc_exp_year, cc_exp_cvv, default_address) VALUES ('John', 'Smith', 'john.smith@example.com', '4321-5678-4321-8765', 10, 2024, 789, 3);

/* orders */
insert into orders (customer_id, amount, delivery_address) values (1, 145.78, 1);

/* order_details */
insert into order_details (order_id, product_id) values (1, 9);
insert into order_details (order_id, product_id) values (1, 4);
insert into order_details (order_id, product_id) values (1, 3);
```

