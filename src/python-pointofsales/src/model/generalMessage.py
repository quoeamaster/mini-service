from pydantic import BaseModel

class GeneralMessage(BaseModel):
    message_type: str
    message_value: str
    status_code: int