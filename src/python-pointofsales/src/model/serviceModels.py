"""
serviceModels

This module provides models used by the Inventory service and Delivery service.
"""
from pydantic import BaseModel
from typing import Optional


class TBDGeo(BaseModel):
    lat: Optional[str] = None
    lng: Optional[str] = None

class TBDCompany(BaseModel):
    name: Optional[str] = None
    catchPhrase: Optional[str] = None
    bs: Optional[str] = None

class TBDAddress(BaseModel):
    street: Optional[str] = None
    suite: Optional[str] = None
    city: Optional[str] = None
    zipcode: Optional[str] = None
    geo: Optional[TBDGeo] = None


# to-do remove non necessary fields and also remove optional typing if necessary
class Product(BaseModel):
    """a model class representing a Product of a shop.
    
    Attributes:
      id (int): the id of the product. (id could be just a running number for mySQL to manage the data)
      product_id (str): the product id of the product. (product_id on the other hand is the real id for identifying the product, which is great for human and device recognition)
      name (str): name of the product. (e.g. men's purse)
      description (str): description of the product. (e.g. Pedro - black men's purse with 4 layers)
      photo_file (str): the image resource representing the product.
      message: (str): error message (if any).
      status_code (int): status code of the error (e.g. 500, 404).
      username: (str): testing data.
      email: (str): testing data.
      phone: (str): testing data.
      website: (str): testing data.
      company: (TBDCompany): testing data.
      address: (TBDAddress): testing data.
    """
    id: Optional[int] = None
    product_id: Optional[str] = None
    name: Optional[str] = None
    description: Optional[str] = None
    photo_file: Optional[str] = None
    # error message related
    message: Optional[str] = None
    status_code: Optional[int] = None
    # these are for mocking purposes only... (tbd)
    username: Optional[str] = None
    email: Optional[str] = None
    phone: Optional[str] = None
    website: Optional[str] = None
    company: Optional[TBDCompany] = None
    address: Optional[TBDAddress] = None


def to_products_from_dict(results):
    """helper function to convert the dict [results] provided back to a model."""
    models = []
    for row in results:
        m = Product()
        m.id = row['id']
        m.product_id = row['product_id']
        m.name = row['name']
        m.description = row['description']
        m.photo_file = row['photo_file']

        models.append(m)

    return models

