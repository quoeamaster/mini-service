# python point-of-sales REST application (fastAPI)

a simple REST enabled application providing api endpoints for integration purpose.

## setup

a virtual environment was setup during development; hence please run the following
```sh
python -m venv path/to/your/repo
```

## activate the virtual env

make sure you are at the root of the python application; then run the following
```sh
source ./bin/activate
```
## deactivate / exit the virtual env

once you have activated the virtual env; simply run the following at any level
```sh
deactivate
```

this command would be placed within your current virtual env session



